%global pypi_name os-win

Summary:        Windows / Hyper-V library for OpenStack projects.
Name:           python-%{pypi_name}
Version:        5.4.0
Release:        7%{?dist}
License:        Apache-2.0
URL:            http://www.cloudbase.it/
Source0:        %{pypi_source}

BuildArch:      noarch

%description
This library contains Windows / Hyper-V code commonly used in the OpenStack projects: nova, cinder,
networking-hyperv. The library can be used in any other OpenStack projects where it is needed.

%package -n python3-%{pypi_name}
Summary:        Windows / Hyper-V library for OpenStack projects.
Provides:       python-%{pypi_name}
BuildRequires: python3-devel python3-setuptools python3-pbr python3-pip python3-wheel python3-eventlet
BuildRequires: python3-oslo-concurrency python3-oslo-config python3-oslo-i18n python3-oslo-log python3-oslo-utils
BuildRequires: python3-ddt python3-oslotest python3-pycodestyle python3-hacking
BuildRequires: python3-testscenarios python3-testtools python3-coverage
Requires: python3-eventlet python3-oslo-concurrency python3-oslo-config python3-oslo-i18n python3-oslo-log
Requires: python3-oslo-utils python3-pbr python3-subunit python3-oslotest python3-os-testr python3-testtools
Requires: python3-testrepository python3-testscenarios

%description -n python3-%{pypi_name}
This library contains Windows / Hyper-V code commonly used in the OpenStack \ projects: nova,
cinder, networking-hyperv. The library can be used in any \ other OpenStack projects where it is needed.

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
cp -arf doc %{buildroot}/%{_pkgdocdir}

%files -n python3-%{pypi_name}
%{python3_sitelib}/os_win
%{python3_sitelib}/*.egg-info
%{_docdir}/*

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.4.0-7
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Tue Sep 10 2024 jackeyji <jackeyji@tencent.com> - 5.4.0-6
- [Type] other
- [DESC] remove useless require python-stestr

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.4.0-5
- Rebuilt for loongarch release

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.4.0-4
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.4.0-3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.4.0-2
- Rebuilt for OpenCloudOS Stream 23.05

* Wed Apr 19 2023 Shuo Wang <abushwang@tencent.com> - 5.4.0-1
- initial build
